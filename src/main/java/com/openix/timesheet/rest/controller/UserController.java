package com.openix.timesheet.rest.controller;

import com.openix.timesheet.rest.response.ComboResponse;
import com.openix.timesheet.service.UserService;
import com.openix.timesheet.configuration.ApiPageable;
import com.openix.timesheet.rest.request.UserLoginRequest;
import com.openix.timesheet.rest.request.UserRequest;
import com.openix.timesheet.rest.response.UserPagedResponse;
import com.openix.timesheet.rest.response.UserResponse;
import com.openix.timesheet.security.TokenContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;
import java.util.NoSuchElementException;
import static com.openix.timesheet.configuration.Constants.*;

@RestController
@RequestMapping(value= "", headers = "Accept=application/json", produces = {"application/json"})
@Transactional
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    private static final String ENTITY = "User";

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @ApiPageable
    @GetMapping("users")
    public ResponseEntity<UserPagedResponse> getAll(@ApiIgnore() @PageableDefault(page = 0, size = 10)Pageable pageable){
        logger.debug(REQUEST_TO_GET_ALL, ENTITY, pageable.getPageNumber(), pageable.getPageSize());
        return ResponseEntity.ok(new UserPagedResponse(userService.findAll(pageable)));
    }

    @PostMapping("login")
    public ResponseEntity login(@RequestBody UserLoginRequest userLogin){
        return ResponseEntity.ok(null);
    }

//    @PostMapping("logOut")
//    public ResponseEntity logOut(){
//        try {
//            return ResponseEntity.ok().build();
//        }catch (NoSuchElementException e){
//            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
//
//        }
//    }

    @PostMapping("users")
    public ResponseEntity<UserResponse> saveUser(@Valid @RequestBody UserRequest user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return ResponseEntity.ok(new UserResponse(userService.save(user.toEntity())));
    }

    @PatchMapping("users/{id}")
    public ResponseEntity<UserResponse> update(@PathVariable Long id, @Valid @RequestBody UserRequest request){
        logger.debug(REQUEST_TO_PUT, ENTITY, id, request);
        return ResponseEntity.ok(new UserResponse(userService.update(id,request.toEntity())));
    }

    @GetMapping("users/{id}")
    public ResponseEntity<UserResponse> getUser(@PathVariable Long id){
        logger.debug(REQUEST_TO_GET, ENTITY, id);
        return ResponseEntity.ok(new UserResponse(userService.findById(id).orElseThrow(NoSuchElementException::new)));
    }

    @GetMapping("account")
    public ResponseEntity<UserResponse> getByToken(){
        logger.debug(REQUEST_TO_GET_BY_TOKEN, ENTITY);
        return ResponseEntity.ok(new UserResponse(userService.findById(TokenContext.getUserId()).orElseThrow(NoSuchElementException::new)));
    }

    @GetMapping("users/combo")
    public ResponseEntity<List<ComboResponse>> getUsersCombo() {
        logger.debug(REQUEST_TO_GET_COMBO, ENTITY);
        List<ComboResponse> combo = userService.findAll(Pageable.unpaged()).map(p -> new ComboResponse(p.getId(), p.getFullName())).getContent();
        return ResponseEntity.ok(combo);
    }
}

