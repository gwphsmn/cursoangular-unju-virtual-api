package com.openix.timesheet.rest.controller;

import com.openix.timesheet.configuration.ApiPageable;
import com.openix.timesheet.rest.request.BaseRequest;
import com.openix.timesheet.service.BaseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

import static com.openix.timesheet.configuration.Constants.*;

@Transactional
@Slf4j
public abstract class BaseController<E, S extends BaseRequest<E>, R> {

    private BaseService<E> service;

    @ApiPageable
    @GetMapping
    public ResponseEntity getAll(@ApiIgnore() @PageableDefault(page = 0, size = 10) Pageable pageable) {
        log.debug(REQUEST_TO_GET_ALL, getEntityName(), pageable.getPageNumber(), pageable.getPageSize());
        return ResponseEntity.ok(buildPagedResponse(getService().findAll(pageable)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<R> get(@PathVariable Long id) {
        log.debug(REQUEST_TO_GET, getEntityName(), id);
        return ResponseEntity.ok(buildFull(getService().get(id)));
    }

    @PostMapping
    public ResponseEntity<R> save(@Valid @RequestBody S request) {
        log.debug(REQUEST_TO_SAVE, getEntityName(), request);
        return ResponseEntity.ok(build(getService().save(request.toEntity())));
    }

    @PutMapping("/{id}")
    public ResponseEntity<R> update(@PathVariable Long id, @RequestBody @Valid S request) {
        log.debug(REQUEST_TO_PUT, getEntityName(), id, request);
        E entity = getService().get(id);
        return ResponseEntity.ok(build(getService().save(request.toEntity(entity))));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable Long id) {
        log.debug(REQUEST_TO_DELETE, getEntityName(), id);
        getService().delete(id);
        return ResponseEntity.ok().build();
    }

    protected abstract String getEntityName();

    protected abstract BaseService<E> getService();

    protected abstract R build(E entity);

    protected abstract R buildFull(E entity);

    protected abstract Object buildPagedResponse(Page<E> page);
}
