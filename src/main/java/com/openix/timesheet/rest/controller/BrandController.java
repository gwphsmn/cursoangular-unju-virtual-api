package com.openix.timesheet.rest.controller;

import com.openix.timesheet.data.entity.Brand;
import com.openix.timesheet.rest.request.BrandRequest;
import com.openix.timesheet.rest.response.BrandPagedResponse;
import com.openix.timesheet.rest.response.BrandResponse;
import com.openix.timesheet.rest.response.ComboResponse;
import com.openix.timesheet.service.BaseService;
import com.openix.timesheet.service.BrandService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.openix.timesheet.configuration.Constants.*;

@RestController
@RequestMapping(value= "brands", headers = "Accept=application/json", produces = {"application/json"})
@Transactional
@Slf4j
public class BrandController extends BaseController<Brand, BrandRequest, BrandResponse> {

    @Autowired
    private BrandService brandService;

    @Override
    protected String getEntityName() {
        return "Brand23";
    }

    @Override
    protected BaseService<Brand> getService() {
        return brandService;
    }

    @Override
    protected BrandResponse build(Brand entity) {
        return new BrandResponse(entity);
    }

    @Override
    protected BrandResponse buildFull(Brand entity) {
        return new BrandResponse(entity);
    }

    @Override
    protected Object buildPagedResponse(Page<Brand> page) {
        return new BrandPagedResponse(page);
    }

    @GetMapping("combo")
    public ResponseEntity<List<ComboResponse>> getCombo() {
        log.debug(REQUEST_TO_GET_COMBO, getEntityName());
        List<ComboResponse> combo = brandService.findAll(Pageable.unpaged()).map(p -> new ComboResponse(p.getId(), p.getName())).getContent();
        return ResponseEntity.ok(combo);
    }

    @GetMapping("combo23")
    public ResponseEntity<List<ComboResponse>> getCombo23() {
        log.debug(REQUEST_TO_GET_COMBO, getEntityName());
        List<ComboResponse> combo = brandService.findAll(Pageable.unpaged()).map(p -> new ComboResponse(p.getId(), p.getName())).getContent();
        return ResponseEntity.ok(combo);
    }
}
