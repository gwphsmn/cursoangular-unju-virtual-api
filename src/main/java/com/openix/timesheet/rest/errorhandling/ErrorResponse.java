package com.openix.timesheet.rest.errorhandling;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ErrorResponse {

    @ApiModelProperty(example = "400")
    private int status;

    @ApiModelProperty(example = "Ocurrio un ERROR")
    private String message;
}
