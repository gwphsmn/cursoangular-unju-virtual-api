package com.openix.timesheet.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.openix.timesheet.data.entity.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter @Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserResponse {

    @ApiModelProperty(example = "1")
    private Long id;

    @ApiModelProperty(example = "Alexis")
    private String firstName;

    @ApiModelProperty(example = "Wayar")
    private String lastName;

    @ApiModelProperty(example = "1990-07-27")
    private LocalDate birthday;

    @ApiModelProperty(example = "alexis@gmail.com")
    private String mail;

    @ApiModelProperty(example = "alexis27")
    private String username;

    @ApiModelProperty(example = "Alexis Wayar")
    private String fullName;

    public UserResponse(User user){
        id = user.getId();
        firstName = user.getFirstName();
        lastName = user.getLastName();
        fullName = user.getFullName();
        birthday = user.getBirthday();
        mail = user.getMail();
        username = user.getName();
    }
}
