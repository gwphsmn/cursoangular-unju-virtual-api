package com.openix.timesheet.rest.response;

import com.openix.timesheet.data.entity.User;
import org.springframework.data.domain.Page;

public class UserPagedResponse extends PageResponse<UserResponse>{

    public UserPagedResponse(Page<User> page){
        super(page);
        data = page.map(UserResponse::new).getContent();
    }
}
