package com.openix.timesheet.rest.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.openix.timesheet.data.entity.Brand;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BrandResponse {

    @ApiModelProperty(example = "1")
    private Long id;

    @ApiModelProperty(example = "Renault")
    private String name;

    @ApiModelProperty(example = "...")
    private String description;

    public BrandResponse(Brand brand) {
        id = brand.getId();
        name = brand.getName();
        description = brand.getDescription();
    }
}
