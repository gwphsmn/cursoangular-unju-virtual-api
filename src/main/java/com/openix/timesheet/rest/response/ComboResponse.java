package com.openix.timesheet.rest.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ComboResponse {

    @ApiModelProperty(example = "1")
    private Long id;

    @ApiModelProperty(example = "name")
    private String name;

    public ComboResponse(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
