package com.openix.timesheet.rest.response;

import com.openix.timesheet.data.entity.Brand;
import org.springframework.data.domain.Page;

public class BrandPagedResponse extends PageResponse<BrandResponse> {

    public BrandPagedResponse(Page<Brand> page){
        super(page);
        data = page.map(BrandResponse::new).getContent();
    }
}
