package com.openix.timesheet.rest.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;

@Getter @ToString
public class UserLoginRequest {

    @NotEmpty
    @ApiModelProperty(example = "openix")
    private String username;

    @NotEmpty
    @ApiModelProperty(example = "openix15")
    private String password;
}
