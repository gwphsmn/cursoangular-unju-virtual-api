package com.openix.timesheet.rest.request;

import com.openix.timesheet.data.entity.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter @Setter @ToString
public class UserRequest {

    @NotEmpty
    @ApiModelProperty(example = "Alexis")
    private String firstName;

    @NotEmpty
    @ApiModelProperty(example = "Wayar")
    private String lastName;

    @NotEmpty
    @ApiModelProperty(example = "alexis@gmail.com")
    private String mail;

    @NotEmpty
    @ApiModelProperty(example = "alexis27")
    private String username;

    @NotEmpty
    @ApiModelProperty(example = "******")
    private String password;

    @NotNull
    @ApiModelProperty(example = "1995-04-27")
    private LocalDate birthday;

    public User toEntity(){
        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setName(username);
        user.setPassword(password);
        user.setMail(mail);
        user.setBirthday(birthday);
        return user;
    }
}
