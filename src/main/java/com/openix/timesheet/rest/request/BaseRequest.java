package com.openix.timesheet.rest.request;

public interface BaseRequest<E> {

    E toEntity();
    E toEntity(E entity);
}
