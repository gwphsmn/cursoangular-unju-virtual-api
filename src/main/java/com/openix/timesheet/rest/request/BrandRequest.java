package com.openix.timesheet.rest.request;

import com.openix.timesheet.data.entity.Brand;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;

@Getter @Setter @ToString
public class BrandRequest implements BaseRequest<Brand> {

    @NotEmpty
    @ApiModelProperty(example = "Fiat")
    private String name;

    @NotEmpty
    @ApiModelProperty(example = "...")
    private String description;

    public Brand toEntity() {
        Brand brand = new Brand();
        brand.setName(name);
        brand.setDescription(description);
        return brand;
    }

    @Override
    public Brand toEntity(Brand brand) {
        brand.setName(name);
        brand.setDescription(description);
        return brand;
    }
}
