package com.openix.timesheet.data.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Getter @Setter
@NoArgsConstructor
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Length(max = 70)
    private String firstName;

    @Length(max = 70)
    private String lastName;

    @Length(max = 30)
    private String name;

    @Length(max = 255)
    private String password;

    @Length(max = 100)
    private String mail;

    private LocalDate birthday;

    public String getFullName() {
        if (firstName == null && lastName == null) return null;
        return firstName + " " + lastName;
    }


//   @ManyToMany(fetch = FetchType.LAZY)
//    @JoinTable(
//            name = "user_role",
//            joinColumns = @JoinColumn(name = "user_id"),
//            inverseJoinColumns = @JoinColumn(name = "role_id"))
//    private Set<Role> roles;

//    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
//    @Transient
//    private Set<Attendance> attendances;
//
//    @ManyToMany(fetch = FetchType.LAZY)
//    @JoinTable(
//            name = "user_project",
//            joinColumns = @JoinColumn(name = "user_id"),
//            inverseJoinColumns = @JoinColumn(name = "project_id"))
//    private Set<Project> projects;
//
//    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
//    @Transient
//    private Set<UserProjectRequest> userProjects;
//
//    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
//    @Transient
//    private Set<Task> tasks;


    public User(Long id) {
        this.id = id;
    }
}
