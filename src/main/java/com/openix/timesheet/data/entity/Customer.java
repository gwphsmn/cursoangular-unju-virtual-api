package com.openix.timesheet.data.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Getter @Setter @NoArgsConstructor
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    @Length(max = 70)
    private String firstName;

    @NotEmpty
    @Length(max = 70)
    private String lastName;

    @NotEmpty
    private String dni;

    @NotNull
    private LocalDate birthdate;

    @Length(max = 100)
    private String email;

    private String address;
}
