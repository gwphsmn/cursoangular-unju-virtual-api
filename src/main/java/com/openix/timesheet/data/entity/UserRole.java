package com.openix.timesheet.data.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
@Entity
@Table(name = "user_role")
@Getter
@Setter
public class UserRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY,targetEntity = User.class)
    private User user;
    @ManyToOne(fetch = FetchType.LAZY,targetEntity = Role.class)
    private Role role;
}
