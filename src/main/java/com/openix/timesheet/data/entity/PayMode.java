package com.openix.timesheet.data.entity;

public enum PayMode {
    CONTADO,
    FINANCIADO
}
