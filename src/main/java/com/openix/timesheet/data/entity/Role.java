package com.openix.timesheet.data.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Role")
@Getter @Setter
public class Role implements Serializable {

    private static final long serialVersionUID = -3026877318989432190L;

    @Id
    @Column(name = "role_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Length(max = 50)
    private String name;

    @Length(max = 255)
    private String description;
}
