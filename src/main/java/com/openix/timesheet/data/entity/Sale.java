package com.openix.timesheet.data.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Getter @Setter @NoArgsConstructor
public class Sale implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id")
    private Customer customer;


    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "car_id")
    private Car car;

    @NotNull
    private Integer discount = 0;

    private Double finalPrice;

    @NotNull
    @Enumerated(EnumType.STRING)
    private PayMode payMode;

    @NotNull
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "seller")
    private User seller;

    private LocalDate saleDate = LocalDate.now();
}
