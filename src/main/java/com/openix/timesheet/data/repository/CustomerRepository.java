package com.openix.timesheet.data.repository;

import com.openix.timesheet.data.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
