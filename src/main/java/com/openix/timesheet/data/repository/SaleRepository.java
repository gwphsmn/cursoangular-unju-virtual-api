package com.openix.timesheet.data.repository;

import com.openix.timesheet.data.entity.Sale;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SaleRepository extends JpaRepository<Sale, Long> {
}
