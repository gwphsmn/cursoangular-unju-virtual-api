package com.openix.timesheet.service;

import com.openix.timesheet.data.entity.Brand;
import com.openix.timesheet.data.repository.BrandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BrandService extends BaseService<Brand> {

    @Autowired
    private BrandRepository brandRepository;

    @Override
    protected BrandRepository getRepository() {
        return brandRepository;
    }
}
