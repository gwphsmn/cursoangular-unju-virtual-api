package com.openix.timesheet.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
@Slf4j
public abstract class BaseService<T> {

    private JpaRepository<T, Long> repository;

    public Page<T> findAll(Pageable pageable) {
        log.debug("findAll - pageable => " + pageable);
        return getRepository().findAll(pageable);
    }

    public T save(T entity) {
        log.debug("save - entity => " + entity);
        return getRepository().save(entity);
    }

    public T get(Long id) {
        log.debug("get - entity ID: " + id);
        return getRepository().findById(id).orElseThrow(NoSuchElementException::new);
    }

    public void delete(Long id){
        log.debug("delete - entity ID: " + id);
        getRepository().deleteById(id);
    }

    protected abstract JpaRepository<T, Long> getRepository();
}
