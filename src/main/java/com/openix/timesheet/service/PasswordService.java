package com.openix.timesheet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Servicio para encodificacion de password
 */
@Service
public class PasswordService {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * Encode Password
     *
     * @param password To encode
     * @return The password encoded
     */
    public String encodePassword(String password) {
        return this.bCryptPasswordEncoder.encode(password);
    }

    /**
     * Check if 2 password match
     *
     * @param rawPassword Raw Password
     * @param encodedPassword Encoded Password
     * @return True if passwords match
     */
    public boolean passwordMatch(String rawPassword, String encodedPassword) {
        return bCryptPasswordEncoder.matches(rawPassword, encodedPassword);
    }


    public boolean isSizeValid(String password) {
        return password.length() >= 8;
    }

}
