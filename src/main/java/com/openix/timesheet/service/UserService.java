package com.openix.timesheet.service;

import com.openix.timesheet.data.entity.User;
import com.openix.timesheet.data.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User get(Long id){
        return userRepository.findById(id).orElseThrow(NoSuchElementException::new);
    }

    public User save(User user){
        return userRepository.save(user);
    }

    public List<User> findAll(){
        return userRepository.findAll();
    }

    public Optional<User> findByUsername(String name){
        return userRepository.findByName(name);
    }

    public User update(Long id, User user){
        User currentUser = get(id);
        currentUser.setName(user.getName());
        currentUser.setPassword(user.getPassword());
        return save(currentUser);
    }

    public void delete(Long id){
        userRepository.deleteById(id);
    }

    public Page<User> findAll(Pageable pageable){
        return userRepository.findAll(pageable);
    }

    public Optional<User> findById(Long id){
        return userRepository.findById(id);
    }
}
