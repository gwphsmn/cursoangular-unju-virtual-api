package com.openix.timesheet.configuration;

public class SecurityConstants {

    public static final String HEADER_STRING = "Authorization";
    public static final String SECRET = "b2ed38ddc864bce4cf58b1731682bb076bd4de01";
    public static final String USER_ID = "UserID";

    public static final String EN_BAD_CREDENTIALS = "Invalid username or password";
}
