package com.openix.timesheet.configuration;

public class Constants {

    // Spring Security

    public static final String HEADER_AUTHORIZACION_KEY = "Authorization";
    public static final String TOKEN_BEARER_PREFIX = "Bearer ";

    // JWT

    public static final String ISSUER_INFO = "https://www.openix.com/";
    public static final String SUPER_SECRET_KEY = "b2ed38ddc864bce4cf58b1731682bb076bd4de01";
    public static final long TOKEN_EXPIRATION_TIME = 864_000_000; // 10 day

    // CONTROLLER
    public static final String REQUEST_TO_GET_BY_TOKEN = "Request to get by token";



    // Log for Service
    public static final String SERVICE_TO_GET_ALL = "Service to get all the {} Page: {} Size: {}";
    public static final String SERVICE_TO_GET = "Service to get a {} ID : {}";
    public static final String SERVICE_TO_FIND = "Service to get a Optional {} ID : {}";
    public static final String SERVICE_TO_SAVE = "Service to save a {} : {}";
    public static final String SERVICE_TO_UPDATE = "Service to update a {} ID: {}";
    public static final String SERVICE_TO_DELETE = "Service to delete a {} ID : {}";
    public static final String SERVICE_TO_GET_ALL_BY_COMPANY = "Service to get all the TradingBook Page: {} Size: {} - Company: {}";
    public static final String SERVICE_TO_GET_ROLES = "Service to get Roles";

    // Log for controller
    public static final String REQUEST_TO_GET_ALL = "Request to get all the {} Page: {} Size: {}";
    public static final String REQUEST_TO_GET_COMBO = "Request to get combo the {}";
    public static final String REQUEST_TO_GET = "Request to get {} ID : {}";
    public static final String REQUEST_TO_SAVE = "Request to save {} : {}";
    public static final String REQUEST_TO_PUT = "Request to update {} ID: {} - {}";
    public static final String REQUEST_TO_DELETE = "Request to delete {} ID : {}";
    public static final String REQUEST_TO_GET_ROLES = "Request to get Roles";

    private Constants() {}
}