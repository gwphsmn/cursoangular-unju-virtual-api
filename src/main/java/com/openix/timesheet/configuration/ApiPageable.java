package com.openix.timesheet.configuration;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.METHOD, ElementType.ANNOTATION_TYPE, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "int", paramType = "query", value = "Número de pagina que quieres obtener desde 0 a N."),
        @ApiImplicitParam(name = "size", dataType = "int", paramType = "query", value = "Número de elementos por página."),
//        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
//                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")
})
public @interface ApiPageable {
}
