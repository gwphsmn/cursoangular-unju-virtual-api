package com.openix.timesheet.configuration;

import com.openix.timesheet.data.repository.UserRepository;
import com.openix.timesheet.security.JWTAuthenticationFilter;
import com.openix.timesheet.security.JWTAuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable().authorizeRequests()
                .antMatchers(
                        "/swagger-ui.html",
                        "/configuration/security",
                        "/v2/api-docs",
                        "/configuration/ui",
                        "/swagger-resources/**",
                        "/webjars/**", "/api/login",
                        "/h2-console/**",
                        "/api/h2-console/**",
                        "/checkIn").permitAll()
                .anyRequest().authenticated().and()
                .addFilter(new JWTAuthenticationFilter(authenticationManager(),userRepository))
                .addFilter(new JWTAuthorizationFilter(authenticationManager(), userRepository));


    }

    /**
     * Habilita el consumo de los  endpoint todos los servidor.
     *
     * @return CorsFilter
     */
    @Bean
    public CorsFilter corsFilter(OpenixProperties properties) {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = properties.getCors();
        if (config.getAllowedOrigins() != null && !config.getAllowedOrigins().isEmpty()) {
            source.registerCorsConfiguration("/**", config);
        }
        return new CorsFilter(source);
    }
}
