package com.openix.timesheet.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.openix.timesheet.data.repository.UserRepository;
import com.openix.timesheet.rest.request.UserLoginRequest;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import static com.openix.timesheet.configuration.Constants.*;
import static com.openix.timesheet.configuration.SecurityConstants.EN_BAD_CREDENTIALS;
import static com.openix.timesheet.configuration.SecurityConstants.USER_ID;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private UserRepository userRepository;

    private AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager, UserRepository userRepository) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        try {
            UserLoginRequest credenciales = new ObjectMapper().readValue(request.getInputStream(), UserLoginRequest.class);

            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    credenciales.getUsername(), credenciales.getPassword(), new ArrayList<>()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {

        ObjectMapper objectMapper = new ObjectMapper();
        String userName = (((User) auth.getPrincipal()).getUsername());
        Optional<com.openix.timesheet.data.entity.User> user = this.userRepository.findByName(userName);

            Claims claims = Jwts.claims().setSubject(userName);
            claims.put(USER_ID, user.get().getId());
            String token = Jwts.builder().setIssuedAt(new Date()).setIssuer(ISSUER_INFO)
                    .setSubject(((User) auth.getPrincipal()).getUsername())
                    .setExpiration(new Date(System.currentTimeMillis() + TOKEN_EXPIRATION_TIME))
                    .setClaims(claims)
                    .signWith(SignatureAlgorithm.HS512, SUPER_SECRET_KEY).compact();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            PrintWriter out = response.getWriter();
            JsonObject newObject = new JsonObject();
            newObject.addProperty("token", token);
            String json = new Gson().toJson(newObject);
            out.print(json);
            out.flush();

    }

}