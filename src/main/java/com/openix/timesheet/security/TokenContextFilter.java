package com.openix.timesheet.security;

import com.openix.timesheet.data.entity.User;
import com.openix.timesheet.data.repository.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Objects;

import static com.openix.timesheet.configuration.Constants.SUPER_SECRET_KEY;
import static com.openix.timesheet.configuration.Constants.TOKEN_BEARER_PREFIX;
import static com.openix.timesheet.configuration.SecurityConstants.HEADER_STRING;
import static com.openix.timesheet.configuration.SecurityConstants.SECRET;
import static com.openix.timesheet.configuration.SecurityConstants.USER_ID;

@Component
public class TokenContextFilter implements Filter {

    private UserRepository userRepository;

    public TokenContextFilter(UserRepository userRepository ){
        this.userRepository = userRepository;
    }


    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String token = ((HttpServletRequest) request).getHeader(HEADER_STRING);
        if(Objects.nonNull(token)){
            Jws<Claims> claims = Jwts.parser().setSigningKey(SUPER_SECRET_KEY).parseClaimsJws(token.replace(TOKEN_BEARER_PREFIX, ""));
//            Jws<Claims> claims = Jwts.parser().setSigningKey(SUPER_SECRET_KEY).parseClaimsJws(token);
            User userRepo = this.userRepository.findById(((Integer) claims.getBody().get(USER_ID)).longValue()).orElseThrow(NoSuchElementException::new);
            TokenContext.setUserId(((Integer) claims.getBody().get(USER_ID)).longValue());
            TokenContext.setCurrentToken(token);
            TokenContext.setUser(userRepo);
        }
        chain.doFilter(request,response);
    }
}
