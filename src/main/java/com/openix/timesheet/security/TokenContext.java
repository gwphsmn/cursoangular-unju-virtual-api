package com.openix.timesheet.security;

import com.openix.timesheet.data.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TokenContext {

    private static Logger logger = LoggerFactory.getLogger(TokenContext.class.getName());
    private static ThreadLocal<ContextEntity> currentThread = new ThreadLocal<>();

    private static ContextEntity getInstance() {

        if (currentThread.get() == null)
            currentThread.set(new ContextEntity());

        return currentThread.get();
    }

    public static void setCurrentToken(String token) {
        logger.debug("Setting tenant to " + token);
        getInstance().setToken(token);
    }

    public static String getCurrentToken() {
        return getInstance().getToken();
    }

    public static void setUserId(Long user) {

        logger.debug("Setting user to " + user);
        getInstance().setUserid(user);
    }

    public static Long getUserId() {
        return getInstance().getUserid();
    }

    public static void clear() {
        currentThread.set(null);
    }

    public static boolean isDefault() {
        return currentThread.get() == null;
    }

    public static void setUser(User user) {
        getInstance().setUser(user);

    }

    public static User getUser() {
        return getInstance().getUser();
    }
}
