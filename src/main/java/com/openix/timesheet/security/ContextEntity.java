package com.openix.timesheet.security;

import com.openix.timesheet.data.entity.User;

public class ContextEntity {

    private Long userid;
    private User user;
    private String token;


    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
