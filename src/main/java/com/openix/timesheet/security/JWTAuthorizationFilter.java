package com.openix.timesheet.security;

import com.openix.timesheet.data.entity.User;
import com.openix.timesheet.data.repository.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Objects;

import static com.openix.timesheet.configuration.Constants.*;
import static com.openix.timesheet.configuration.SecurityConstants.SECRET;
import static com.openix.timesheet.configuration.SecurityConstants.USER_ID;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private UserRepository userRepository;

    public JWTAuthorizationFilter(AuthenticationManager authManager, UserRepository userRepository) {
        super(authManager);
        this.userRepository = userRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        String header = req.getHeader(HEADER_AUTHORIZACION_KEY);
//        if (header == null || !header.startsWith(TOKEN_BEARER_PREFIX)) {
        if (header == null) {
            chain.doFilter(req, res);
            return;
        }
        UsernamePasswordAuthenticationToken authentication = getAuthentication(req);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_AUTHORIZACION_KEY);
        if (token != null) {
            // Se procesa el token y se recupera el usuario.
            String user = Jwts.parser()
                    .setSigningKey(SUPER_SECRET_KEY)
                    .parseClaimsJws(token.replace(TOKEN_BEARER_PREFIX, ""))
                    .getBody()
                    .getSubject();

            Jws<Claims> claims = Jwts.parser().setSigningKey(SUPER_SECRET_KEY).parseClaimsJws(token.replace(TOKEN_BEARER_PREFIX, ""));
            User userRepo = TokenContext.getUser();
            if(Objects.isNull(userRepo)){
                userRepo = this.userRepository.findById(new Long((Integer) claims.getBody().get(USER_ID))).get();
                TokenContext.setUser(userRepo);
            }
            TokenContext.setUserId(new Long((Integer) claims.getBody().get(USER_ID)));
            TokenContext.setCurrentToken(token);
            //User userss = TokenContext.getUser();
            if (user != null) {
                return new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
            }
            return null;
        }
        return null;
    }
}