INSERT INTO public.user (first_name, last_name, name, password, mail, birthday)
VALUES('Alexis', 'Wayar', 'openix', '$2a$10$TQOqXVHP9BgfOK0/.2LOfe0vUXZVSZDc6vuLKfGt0ndnnh9Uf.q1e', 'alexis27@gmail.com', '1995-07-27');

INSERT INTO public.user (first_name, last_name, name, password, mail, birthday)
VALUES('Noelia', 'Tapia', 'noelia', '$2a$10$TQOqXVHP9BgfOK0/.2LOfe0vUXZVSZDc6vuLKfGt0ndnnh9Uf.q1e', 'noelia@gmail.com', '1990-05-21');

INSERT INTO brand (id, name, description) VALUES(1, 'Renault', '...');
